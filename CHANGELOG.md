# CHANGELOG

## [v1.1] 2021-02-03

- feat: 增加`shaun-togglz`模块,对`togglz-console`页面进行安全拦截
- feat: 新增`ShiroAuthorityManager`类,使用`shiro`对`permission`进行校验